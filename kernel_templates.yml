---

# Default kernel repo workflow
.workflow:
  rules:
    - if: '$CI_MERGE_REQUEST_ID && $CI_PROJECT_ID != $CI_MERGE_REQUEST_PROJECT_ID'
      variables:
        REQUESTED_PIPELINE_TYPE: 'insufficient-permissions'
    - if: '$CI_MERGE_REQUEST_PROJECT_PATH =~ /^redhat.rhel/ || $CI_PROJECT_PATH =~ /^redhat.rhel/'
      variables:
        REQUESTED_PIPELINE_TYPE: 'internal'
    - if: '$CI_MERGE_REQUEST_PROJECT_PATH =~ /^redhat.centos-stream/ || $CI_PROJECT_PATH =~ /^redhat.centos-stream/ ||
           $CI_MERGE_REQUEST_PROJECT_PATH =~ /^CentOS/ || $CI_PROJECT_PATH =~ /^CentOS/'
      variables:
        REQUESTED_PIPELINE_TYPE: '/^(trusted|centos-rhel)$/'
    - if: '$CI_MERGE_REQUEST_PROJECT_PATH =~ /^redhat.prdsc/ || $CI_PROJECT_PATH =~ /^redhat.prdsc/'
      variables:
        REQUESTED_PIPELINE_TYPE: 'scratch'

# explicitly fail pipelines running in a fork instead of the parent repo
# most likely a click on https://red.ht/GitLabSSO is needed for RH employees
insufficient-permissions:
  trigger:
    project: redhat/red-hat-ci-tools/kernel/insufficient-permissions
    strategy: depend
  rules:
    - if: '$REQUESTED_PIPELINE_TYPE == "insufficient-permissions"'

# Merge request pipeline
.merge_request:
  variables:
    title: ${CI_COMMIT_TITLE}
    commit_hash: ${CI_COMMIT_SHA}
    git_url: ${CI_MERGE_REQUEST_PROJECT_URL}.git
    branch: ${CI_MERGE_REQUEST_TARGET_BRANCH_NAME}
    mr_id: ${CI_MERGE_REQUEST_IID}
    mr_url: ${CI_MERGE_REQUEST_PROJECT_URL}/-/merge_requests/${CI_MERGE_REQUEST_IID}
    mr_project_id: ${CI_MERGE_REQUEST_PROJECT_ID}
    mr_source_branch_hash: ${CI_MERGE_REQUEST_SOURCE_BRANCH_SHA}  # only set on (non-draft) merged result pipelines
    mr_diff_base_hash: ${CI_MERGE_REQUEST_DIFF_BASE_SHA}
    mr_pipeline_id: ${CI_PIPELINE_ID}
    trigger_job_name: ${CI_JOB_NAME}
  rules:
    - if: '$PIPELINE_TYPE =~ $REQUESTED_PIPELINE_TYPE && $CI_MERGE_REQUEST_ID && (
             ($RUN_ONLY_FOR_RT =~ /^[Tt]rue$/ && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME =~ /-rt$/) ||
             ($RUN_ONLY_FOR_AUTOMOTIVE =~ /^[Tt]rue$/ && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME =~ /-automotive$/) ||
             (
               ($RUN_ONLY_FOR_RT !~ /^[Tt]rue$/ && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME !~ /-rt$/) &&
               ($RUN_ONLY_FOR_AUTOMOTIVE !~ /^[Tt]rue$/ && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME !~ /-automotive$/)
             )
          )'

# Baseline pipeline
.baseline:
  variables:
    title: ${CI_COMMIT_TITLE}
    commit_hash: ${CI_COMMIT_SHA}
    git_url: ${CI_PROJECT_URL}.git
    branch: ${CI_COMMIT_BRANCH}
    test_set: 'kt1'
    scratch: 'false'
    trigger_job_name: ${CI_JOB_NAME}
  rules:
    - if: '$PIPELINE_TYPE =~ $REQUESTED_PIPELINE_TYPE && $CI_COMMIT_BRANCH && (
             ($RUN_ONLY_FOR_RT =~ /^[Tt]rue$/ && $CI_COMMIT_BRANCH =~ /-rt$/) ||
             ($RUN_ONLY_FOR_AUTOMOTIVE =~ /^[Tt]rue$/ && $CI_COMMIT_BRANCH =~ /-automotive$/) ||
             (
               ($RUN_ONLY_FOR_RT !~ /^[Tt]rue$/ && $CI_COMMIT_BRANCH !~ /-rt$/) &&
               ($RUN_ONLY_FOR_AUTOMOTIVE !~ /^[Tt]rue$/ && $CI_COMMIT_BRANCH !~ /-automotive$/)
             )
          )'

# Trigger internal child pipeline
.internal:
  trigger:
    project: redhat/red-hat-ci-tools/kernel/cki-internal-pipelines/cki-internal-contributors
    strategy: depend
  variables:
    PIPELINE_TYPE: 'internal'
    publish_elsewhere: 'true'  # needs to match the pipeline project in trigger/project!
    # use fixed git cache owner as group of kernel repositories might differ from 'kernel'
    git_url_cache_owner: kernel
    merge_tree_cache_owner: kernel

.centos_stream_rhel_internal:
  extends: .internal
  variables:
    PIPELINE_TYPE: 'centos-rhel'

# Trigger trusted child pipeline
.trusted:
  trigger:
    project: redhat/red-hat-ci-tools/kernel/cki-internal-pipelines/cki-trusted-contributors
    strategy: depend
  variables:
    PIPELINE_TYPE: 'trusted'

# Trigger scratch child pipeline
.scratch:
  trigger:
    project: redhat/red-hat-ci-tools/kernel/cki-scratch-pipelines/scratch-pipelines
    strategy: depend
  variables:
    PIPELINE_TYPE: 'scratch'
    publish_elsewhere: 'true'  # needs to match the pipeline project in trigger/project!
    git_url_cache_owner: kernel
    merge_tree_cache_owner: kernel
    skip_results: 'true'
    test_priority: 'urgent'

# Only build and publish, exclude setup, test and results
.only_build_and_publish:
  variables:
    skip_setup: 'true'  # No need to send pre-test messages
    skip_test: 'true'  # Skip testing, only need to look out for merge/build problems
    skip_results: 'true'  # Any issues should be caused by conflicts

# Obsolete, but in use and cannot be empty
.with_notifications:
  variables: {}

# Common RHEL settings
.rhel_common:
  variables:
    test_debug: 'true'
    rpmbuild_with: 'up zfcpdump'
    source_package_name: kernel

# Common realtime_check settings
.realtime_check_common:
  extends: [.rhel_common]
  allow_failure: true  # Don't block MRs!
  variables:
    package_name: kernel-rt
    source_package_name: kernel-rt
    rpmbuild_with: 'up realtime'
    merge_tree: ${CI_MERGE_REQUEST_PROJECT_URL}.git
    merge_branch: ${CI_MERGE_REQUEST_TARGET_BRANCH_NAME}-rt
    architectures: 'x86_64'  # Supported on x86_64 only

# Common full RT pipeline settings (<9.3)
.realtime_pipeline_common:
  extends: .rhel_common
  variables:
    package_name: kernel-rt
    source_package_name: kernel-rt
    rpmbuild_with: 'up realtime'
    architectures: 'x86_64'  # Supported on x86_64 only

# Common kernel-rt settings (>=9.3)
.rt_common:
  extends: .rhel_common
  variables:
    package_name: kernel-rt
    source_package_name: kernel
    rpmbuild_with: 'realtime'
    architectures: 'x86_64'  # Supported on x86_64 only

# Common kernel-64k settings
.64k_common:
  variables:
    package_name: kernel-64k
    source_package_name: kernel
    architectures: 'aarch64'
    rpmbuild_with: 'arm64_64k'
    kpet_extra_components: '64kpages'

# Common kernel-debug settings
.debug_common:
  variables:
    package_name: kernel-debug
    source_package_name: kernel
    rpmbuild_with: 'debug'
    kpet_extra_components: 'debugbuild'

# Common full automotive pipeline settings
.automotive_pipeline_common:
  extends: .rhel_common
  variables:
    package_name: kernel-automotive
    source_package_name: kernel-automotive
    architectures: 'x86_64 aarch64'  # Supported on x86_64/arm only
    test_runner: aws
    test_set: automotive
    AWS_UPT_IMAGE_OWNER: '587138297281'
    AWS_UPT_LAUNCH_TEMPLATE_NAME: 'arr-cki.prod.lt.upt-automotive'
    AUTOMOTIVE_CONFIGURATION_URL: 'https://autosd.sig.centos.org/AutoSD-9/nightly/sample-images/AMI_info_cki_{arch}.json'

# Automotive check configs
.automotive_check_common:
  extends: .automotive_pipeline_common
  allow_failure: true  # Don't block MRs!
  variables:
    merge_tree: ${CI_MERGE_REQUEST_PROJECT_URL}.git
    merge_branch: ${CI_MERGE_REQUEST_TARGET_BRANCH_NAME}-automotive

# Coverage variables
.coverage:
  variables:
    coverage: 'true'
    test_debug: 'false'  # Override from .rhel_common because it's not supported by specfile
